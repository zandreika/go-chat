package main

import (
	"../router"
)

var routesHandlers = map[string]router.RouteHandler{
	"/auth/login": handleAuthLogin,
	"/message":    handleBroadcastMessage,
	"/exit":       handleExit,
}
