package postman

import (
	"errors"
	"net"

	"../messages"
)

// PostmanEmptyMessageError - specific string for empty message error
var PostmanEmptyMessageError = "Empty message"

// Postman - struct for send and recieve messages
type Postman struct {
	Conn   net.Conn
	isAuth bool
}

// New - constructor for Postman
func New(c net.Conn) *Postman {
	return &Postman{Conn: c, isAuth: false}
}

// SetAuthenticate - set authentication status
func (p *Postman) SetAuthenticate(value bool) {
	p.isAuth = value
}

// IsAuthenticated - check is postman authenticated
func (p *Postman) IsAuthenticated() bool {
	return p.isAuth
}

// Receive - read bytes and deserialize it to message
func (p *Postman) Receive() (messages.Message, error) {
	var resMsg messages.Message

	readBytes := make([]byte, 1024)

	n, err := p.Conn.Read(readBytes)
	if err != nil {
		return resMsg, err
	}

	if n <= 0 {
		return resMsg, errors.New("Empty message")
	}

	recvMsg := make([]byte, 1024)
	recvMsg = readBytes[:n]

	resMsg, err = messages.Deserialize(recvMsg)
	return resMsg, err
}

// Send - serialize message and send bytes array
func (p *Postman) Send(msg messages.Message) {
	serializedMsg, _ := messages.Serialize(msg)
	p.Conn.Write(serializedMsg)
}

// SendBytes - send bytes array
func (p *Postman) SendBytes(bytes []byte) {
	p.Conn.Write(bytes)
}

// Dismiss - close connection
func (p *Postman) Dismiss() {
	p.Conn.Close()
}
